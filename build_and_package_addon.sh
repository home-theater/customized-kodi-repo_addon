#!/usr/bin/env bash
set -e

function clean {
  if [ -e common.d ]; then rm common.d; fi
  if [ -e build_and_package_addon.d ]; then rm build_and_package_addon.d; fi
}

clean

curl -Ssf https://codeberg.org/home-theater/scripts/raw/branch/master/common.d -o common.d
curl -Ssf https://codeberg.org/home-theater/scripts/raw/branch/master/build_and_package_addon.d -o build_and_package_addon.d
chmod +x build_and_package_addon.d

./build_and_package_addon.d

clean
