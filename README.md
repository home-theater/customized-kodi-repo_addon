# Home theater repository addon

## Build

Convert `{"defines": [], "substitutions": {"baseurl": "https://home-theater.example.com", "password": "example"}}` to base64.

```
./build_and_package.d eyJkZWZpbmVzIjogW10sICJzdWJzdGl0dXRpb25zIjogeyJiYXNldXJsIjogImh0dHBzOi8vaG9tZS10aGVhdGVyLmV4YW1wbGUuY29tIiwgInBhc3N3b3JkIjogImV4YW1wbGUifX0=
```

